Ansible Role: Mega CMD
=========

Install [Mega CMD](https://mega.nz/cmd) ([git](https://github.com/meganz/MEGAcmd)) on **Debian** and **Ubuntu**

Requirements
--------------

None.

Role Variables
--------------

Mandatory variables:
```yaml
megacmd_username: <your email account>
megacmd_password: <your password account>
```

Dependencies
------------

None.

Example Playbook
----------------

    - hosts: servers
      roles:
         - role-mega-cmd

License
-------

BSD

Author Information
------------------

This role was created in 2021 by [Yohann Valentin](https://yohannvalentin.com/)